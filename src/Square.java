import java.util.ArrayList;

import javafx.geometry.Bounds;
import javafx.scene.image.Image;

import javafx.scene.input.KeyCode;

public class Square extends Actor{
    double m ,jumpVel, runJump, rX, rY, xAccel, fri;

    public Image image;
    boolean clearStage, isMove, f;




    ArrayList<Integer> i;
    public Square() {
        
       
        image = new Image("circle.png" , 25,25,false,false);
        m = 5;
        
        this.setImage(image);
        this.setX(100);
        this.setY(100);
        
        
        
    }
    
    @Override
    public void act(long now) {
      
        moveX();
        moveY();
        
        
        
    }
    
    public void moveX() {
       
        if(getWorld().isKeyDown(KeyCode.RIGHT)  ) {
          
          this.setX(this.getX() + m);
          
            

        
        
        }else if(getWorld().isKeyDown(KeyCode.LEFT) ) {
           this.setX(this.getX() - m);
            
        }
        
    }
    
    public void moveY() {
        
      if(getWorld().isKeyDown(KeyCode.UP)  ) {
        
        
         this.setY(this.getY() - m);
        

    
    
      }else if(getWorld().isKeyDown(KeyCode.DOWN) ) {
        
        this.setY(this.getY() + m);
      }
    
    }
    
    
    
    public boolean isClearStage() {
        return clearStage;
    }
    
    public void setClearStage(boolean s) {
        clearStage = s;
    }
    
    public void setPos() {
      int col = (int)((this.getX()-this.getWidth()*2)/this.getHeight());
      int row = (int)((this.getY()- this.getHeight()- this.getHeight()-this.getHeight()*2)/this.getHeight());
      getWorld().setSquarePos(col, row);
    }
    

    
    
    
    
    

}

