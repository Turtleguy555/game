import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;

public abstract class World extends Pane{
    private AnimationTimer timer;
    long lastUp;
    HashSet keys;
    HashSet numNodes;
    int counter;
    int xCounter;
    boolean flag;
    
    int xPos;
    int yPos;
    
    //ArrayList<Obstacles> o;
    public World() {
         //o = new ArrayList<Obstacles>();
         numNodes = new HashSet();
         keys = new HashSet();
         flag = false;
         xPos = 0;
         yPos = 0;
         
         
         
         
         
         timer = new AnimationTimer() {
             
            @Override

            public void handle(long now) {
              
              
                if((now - lastUp) >= 0) {
                    try {
                        for(int i = 0; i < getChildren().size()-numNodes.size(); i++) {
                            getObjects(Actor.class).get(i).act(now);
                        }
                    }catch(IndexOutOfBoundsException e) {
                        
                    }
                    counter++;
                    xCounter++;
                    lastUp = now;
                
                }
                
                
            }
        };
        
            
        
        
    }
    
    public long getTime() {
        return counter;
    }
    
    
    
    public long resetTime() {
        return counter = 0;
    }
    
    public boolean getFlag() {
        return flag;
    }
    
    public void setFlag(boolean f) {
        flag = f;
    }
    
    public long getXTime() {
        return xCounter;
    }
    
    public void resetXTime() {
        xCounter = 0;
    }
    
    public abstract void act(long now);
    
    public void add(Actor actor) {

        this.getChildren().add(actor);
    

        
    } 

    
    
    public <A extends Actor>List<A> getObjects(Class<A> cls){
        ArrayList<A> arr = new ArrayList<A>();
        for(int i = 0; i < this.getChildren().size(); i++) { 
            //if(this.getChildren().get(i).getClass().isAssignableFrom(cls)) {
                //arr.add((A) this.getChildren().get(i));
            //}
        
            try {
                if(cls.isInstance((Actor)getChildren().get(i)) ) {
                    arr.add((A) getChildren().get(i));
                    
                }
            }catch(java.lang.ClassCastException e) {
                numNodes.add(getChildren().get(i));
                
                
            }
            
            
            
            
                
            
            
    
                
        }
        
    
        //System.out.println(getChildren().size());
        return arr;
    }
    
    public void addKeys(KeyCode k) {
        keys.add(k);
    }
    
    public void removeKey(KeyCode k) {
        keys.remove(k);
    }
    
    public boolean isKeyDown(KeyCode k) {
        if(keys.contains(k)) {
            return true;
        }
        return false;
    }
    
        
    
    
    public void remove(Actor actor) {
        this.getChildren().remove(actor);
    }
    
    public void start() {
        timer.start();
        
    }
    
    public void stop() {
        timer.stop();
        
    }
    
    public void setSquarePos(int x, int y) {
      xPos = x;
      yPos = y;
      
    }
    
    
    
}

