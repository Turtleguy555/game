import java.util.ArrayList;

import javafx.scene.image.ImageView;

public abstract class Actor extends ImageView{
    
    public Actor() {
        
    }
    
    public abstract void act(long now);
    
    public double getHeight() {
        return getImage().getHeight();
    }
    
    public <A extends Actor>java.util.List<A> getIntersectingObjects(Class<A> cls){
        ArrayList<A> bob = new ArrayList<A>();
        for(int i = 0; i < getWorld().getObjects(cls).size(); i++) {
            
            
            if(this.intersects(getWorld().getObjects(cls).get(i).getBoundsInParent())) {
                //System.out.println(i);
                bob.add(getWorld().getObjects(cls).get(i));
            }
            
            
        
                
            
        }
        return bob;
        
    }
    
    
    
    
    
    //public int getBrick(){
        //int counter = -1;
//      for(int i = 0; i < getWorld().getObjects(Brick.class).size(); i++) {
//          
//          
//          if((this.getX() >= getWorld().getObjects(Brick.class).get(i).getX() && this.getX() <= getWorld().getObjects(Brick.class).get(i).getX() + getWorld().getObjects(Brick.class).get(i).getWidth()) && (this.getY() <= getWorld().getObjects(Brick.class).get(i).getY() + getWorld().getObjects(Brick.class).get(i).getHeight()  &&  (this.getY() >= getWorld().getObjects(Brick.class).get(i).getY()                       ))){
//              counter = i;
//              
//          }
//          
//          
//      
//              
//          
//      }
//      return counter;
//      
//  }
    
    
    
    
    public <A extends Actor>A getOneIntersectingObject(Class<A> cls){
        //System.out.println(getWorld().getObjects(Actor.class).size());
        if(getWorld().getObjects(cls).size() > 0) {
            
            if(this.intersects(getWorld().getObjects(cls).get(0).getBoundsInParent())){         
                
                return getWorld().getObjects(cls).get(0);
            }else {
                return null;
            }
            
        }
        return null;
        
                
            
    
        
        
    }
    
    public double getWidth() {
        return getImage().getWidth();
    }
    
    public World getWorld() {
        return (World)(getParent());
    }
    
    public void move(double dx, double dy) {
        
        setX(getX() + dx);
        setY(getY() + dy);
    }
    
    
    
}
