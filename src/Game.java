

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Game extends Application {
  GridPane g;
  Square square;
  Stage stage;
  BorderPane root;
  Scene scene;
  BallWorld world;
  
  
  

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage s) throws Exception {

    stage = s;
    stage.setResizable(false);
    stage.sizeToScene();
    root = new BorderPane();
    scene = new Scene(root, 1000, 600);
    stage.setScene(scene);
    world = new BallWorld();
    root.setCenter(world);
    
    
    
    
    square = new Square();
    world.add(square);
    world.requestFocus();
    world.start();
    
    
    
    
    

    



    world.setOnKeyPressed(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent e) {

        if (e.getCode() == KeyCode.RIGHT
            && !(square.getX() > scene.getWidth() - square.getWidth())) {
          
          world.addKeys(KeyCode.RIGHT);
        } else {
          world.removeKey(KeyCode.RIGHT);
        }
        if (e.getCode() == KeyCode.LEFT && !(square.getX() < 0)) {

          world.addKeys(KeyCode.LEFT);
        } else {
          world.removeKey(KeyCode.LEFT);
        }
        if (e.getCode() == KeyCode.UP) {
          world.addKeys(KeyCode.UP);
        } else {
          world.removeKey(KeyCode.UP);
        }
        if (e.getCode() == KeyCode.DOWN) {
          world.addKeys(KeyCode.DOWN);
        } else {
          world.removeKey(KeyCode.DOWN);
        }

      }


    });

    world.setOnKeyReleased(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent e) {
        if (e.getCode() == KeyCode.RIGHT
            && !(square.getX() > scene.getWidth() - square.getWidth())) {
          world.removeKey(KeyCode.RIGHT);
        }
        if (e.getCode() == KeyCode.LEFT && !(square.getX() < 0)) {
          world.removeKey(KeyCode.LEFT);
        }

        if (e.getCode() == KeyCode.UP) {
          world.removeKey(KeyCode.UP);
        }

        if (e.getCode() == KeyCode.DOWN) {
          world.removeKey(KeyCode.DOWN);
        }
      }
    });
    
    stage.show();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
