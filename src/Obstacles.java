
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;


public class Obstacles extends Actor {
    boolean danger;
    public Obstacles(String s) {
        danger = false;
        this.setImage(new Image(s)); 
    }
    @Override
    public void act(long now) {
        // TODO Auto-generated method stub
        
    }
    
    public boolean getDanger() {
        return danger;
    }
    
    public void setDanger(boolean d) {
        danger = d;
    }
    
    public void setXDim(double x) {
        this.setScaleX(x);
    }
    
    public void setYDim(double y) {
        this.setScaleY(y);
    }
    
    

    

    

}

